FROM gradle:5.5-jdk8

RUN git clone https://gitlab.com/glycosmos/testapibywfw.git \
&& cd testapibywfw && gradle mkJar 
RUN ls

ENTRYPOINT exec java -jar ./testapibywfw/build/libs/testapibywfw.jar
