package run;

import java.util.LinkedList;
import static spark.Spark.*;
import org.glycoinfo.WURCSFramework.util.WURCSValidator;

public class main {
  public static void main(String[] args) {
    get("/WURCS/:str", (req, res) ->{ 
		String t_strWURCS = req.params(":str");



   // System.out.println(t_strWURCS+": ");
    WURCSValidator validator = new WURCSValidator();
    validator.start(t_strWURCS);
    String retStr = "the number of errors: " + validator.getTheNumberOfErrors();
    
    if(validator.getTheNumberOfErrors()==0){
      if(validator.getStandardWURCS().equals(t_strWURCS)){
      System.out.println("No need for normalization.");
      retStr = retStr + " -  " +"No need for normalization.";
      }else{
      System.out.println(validator.getStandardWURCS());
      retStr = retStr + " - " + validator.getStandardWURCS();
      }
    }

    for(String er: validator.getErrors()){
      retStr = retStr + " - " + "	Error:   "+er;
    }
    
    for(String wa: validator.getWarnings()){
      retStr = retStr + " - " + "	Warning: "+wa;
    }

return retStr;
    });
  }
}
