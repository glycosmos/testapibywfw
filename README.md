# open api
https://wfwtest.glycosmos.org/WURCS/WURCS%3D2.0%2F1%2C1%2C0%2F%5Bu2122h%5D%2F1%2F

# doValidate
WFWをAPIで使いたい時の一例。

# Install  
install java8  
git clone https://gitlab.com/glycosmos/testapibywfw.git  
cd testapibywfw  
gradle mkJar    

# run  
It's deamon.  
java -jar build/libs/doWurcsValidator.jar 

# doValidate
Please access the URL below.  
http://localhost:4567/WURCS/WURCS%3D2.0%2F1%2C1%2C0%2F%5Bu2122h%5D%2F1%2F  

![result](https://gitlab.com/glycosmos/gly-develop/raw/master/sampleProject/doWurcsValidator/result.png)  

# On Docker   
git clone https://gitlab.com/glycosmos/testapibywfw.git  
cd testapibywfw  
docker-compose up -d  

Please access the URL below.  
http://localhost:4567/WURCS/WURCS%3D2.0%2F1%2C1%2C0%2F%5Bu2122h%5D%2F1%2F  

# Development
plrease edit ./src/main/java/run/main.java
